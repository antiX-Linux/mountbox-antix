��          t      �                 .     K     c     {     �     �  a   �     �       �       �  "   �          !     ;     C     I  p   P     �     �                                                  
      	    <b>Device             : </b> <b>Harddisk devices list</b> <b>Mount point   : </b> <b>antiX - Mountbox</b> Configuration List Mount Mount device in /media. Device can be flash key, cd/dvd or any USB device. Unmount device in rox. Partitions table Umount Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-16 16:04+0300
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 <b>Enhet             : </b> <b>Liste over harddisk-enheter</b> <b>Monteringspunkt   : </b> <b>antiX – Mountbox</b> Oppsett Liste Monter Monter enhet under /media. Type enhet kan være minnepinne, CD/DVD eller en USB-enhet. Avmonter enheten med rox. Partisjonstabell Avmonter 